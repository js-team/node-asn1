# Installation
> `npm install --save @types/asn1`

# Summary
This package contains type definitions for asn1 ( https://github.com/joyent/node-asn1 ).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/asn1

Additional Details
 * Last updated: Wed, 30 Jan 2019 18:47:31 GMT
 * Dependencies: @types/node
 * Global values: none

# Credits
These definitions were written by Jim Geurts <https://github.com/jgeurts>.
